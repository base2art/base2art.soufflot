﻿namespace Base2art.Soufflot.Diagnostics.Logging
{
    public interface ILogger
    {
        void Log(string message, LogLevel level);
    }
}