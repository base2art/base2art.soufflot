﻿namespace Base2art.Soufflot.Api
{
    using System;
    using System.Linq.Expressions;

    using System.Reflection;
    using Base2art.Soufflot.Mvc;

    public interface IRouteData
    {
        Type ControllerClass { get; }
        
        MethodInfo Method { get; }
        
        object[] Parameters { get; }
        
        Expression<Func<Type, MethodInfo, object[], IResult>> Expression { get; }
    }
}