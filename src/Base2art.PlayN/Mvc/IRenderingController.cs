﻿namespace Base2art.PlayN.Mvc
{
    using System.Collections.Generic;

    using Base2art.PlayN.Api;
    using Base2art.PlayN.Http;

    public interface IRenderingController
    {
        IPositionedRenderingController[] RenderingControllers { get; }

        INonRenderingController[] NonRenderingControllers { get; }

        IResult Execute(IHttpContext httpContext, List<PositionedResult> childResults);
    }
}