namespace Base2art.PlayN.Mvc
{
    public interface IPositionedRenderingController
    {
        IRenderingController RenderingController { get; }

        int Container { get; }

        int ContainerPriority { get; }
    }
}