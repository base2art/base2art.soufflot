namespace Base2art.PlayN.Mvc
{
    using Base2art.PlayN.Http;

    public interface INonRenderingController
    {
        INonRenderingController[] NonRenderingControllers { get; }

        void Execute(IHttpContext httpContext);
    }
}