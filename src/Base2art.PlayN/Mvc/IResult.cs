﻿namespace Base2art.PlayN.Mvc
{
    public interface IResult
    {
        IResult As(string contentType);

        IContent Content { get; }
    }
}