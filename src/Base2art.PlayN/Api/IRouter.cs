﻿namespace Base2art.PlayN.Api
{
    using System.Collections.Generic;

    using Base2art.PlayN.Mvc;

    using Base2art.PlayN.Http;

    public interface IRouter
    {
        IRouteData<IRenderingController> FindRenderingControllerType(IHttpRequest request);

        IEnumerable<IRouteData<INonRenderingController>> FindNonRenderingControllerTypes(IHttpRequest request);
    }
}