﻿namespace Base2art.PlayN.Api.Diagnostics
{
    public interface ILogger
    {
        void Log(string message, LogLevel level);
    }
}