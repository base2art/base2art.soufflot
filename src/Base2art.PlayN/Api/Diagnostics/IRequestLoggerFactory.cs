﻿namespace Base2art.PlayN.Api.Diagnostics
{
	public interface IRequestLoggerFactory
	{
		ILogger Create(LogLevel logLevel);
	}
}


