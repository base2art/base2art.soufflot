﻿namespace Base2art.PlayN.Api
{
    using Base2art.PlayN.Api;
    using Base2art.PlayN.Api.Config;

    public interface IApplicationBuilder
    {
        IApplication BuildApplication(ApplicationMode mode, string rootDirectory, IConfigurationProvider configProvider);
    }
}