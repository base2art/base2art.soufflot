﻿namespace Base2art.PlayN.Api
{
    using System.Collections.Generic;

    using Base2art.PlayN.Http;
    using Base2art.PlayN.Mvc;

    public sealed class NullRouter : IRouter
    {
        public IRouteData<IRenderingController> FindRenderingControllerType(IHttpRequest request)
        {
            return null;
        }

        public IEnumerable<IRouteData<INonRenderingController>> FindNonRenderingControllerTypes(IHttpRequest request)
        {
            return null;
        }
    }
}