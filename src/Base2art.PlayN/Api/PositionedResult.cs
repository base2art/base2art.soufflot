﻿namespace Base2art.PlayN.Api
{
    
    public class PositionedResult
    {
        public SimpleResult Result { get; set; }

        public int Container { get; set; }

        public int ContainerPriority { get; set; }
    }
}