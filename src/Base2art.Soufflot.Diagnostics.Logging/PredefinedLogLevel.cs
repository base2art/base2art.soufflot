﻿
using System;

namespace Base2art.Soufflot.Diagnostics.Logging
{
    public class PredefinedLogLevel : LogLevel
    {
        public PredefinedLogLevel(PredefinedLogLevels predefinedLogLevel)
            : base((byte)predefinedLogLevel, predefinedLogLevel.ToString("G"), predefinedLogLevel.ToString("G"))
        {
        }

    }
}
