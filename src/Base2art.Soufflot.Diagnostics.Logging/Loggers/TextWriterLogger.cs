﻿namespace Base2art.Soufflot.Diagnostics.Logging.Loggers
{
    using System;
    using System.IO;

    public class TextWriterLogger : LoggerBase
    {
        private readonly TextWriter logger;

        public TextWriterLogger(TextWriter logger, LogLevel level)
            : base(level)
        {
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }
            
            this.logger = logger;
        }
        
        protected override void LogMesssage(ILogMessage logMessage)
        {
            this.logger.WriteLine(logMessage);
            this.logger.Flush();
        }
    }
}