﻿namespace Base2art.Soufflot.Diagnostics.Logging.Loggers
{
    public class NullLogger : ILogger
    {
        public void Log(string message, LogLevel level)
        {
        }
    }
}