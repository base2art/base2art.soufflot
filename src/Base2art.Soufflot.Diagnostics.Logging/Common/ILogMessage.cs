﻿namespace Base2art.Soufflot.Diagnostics.Logging
{
    public interface ILogMessage
    {
        LogLevel Level { get; }

        string Message { get; }
    }
}