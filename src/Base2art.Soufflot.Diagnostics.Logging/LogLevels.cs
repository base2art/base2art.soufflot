﻿namespace Base2art.Soufflot.Diagnostics.Logging
{
    public static class LogLevels
    {
        public static readonly LogLevel Always = new PredefinedLogLevel(PredefinedLogLevels.Always);

        // 1-63
        // 0x01 - 0x3F
        public static readonly LogLevel SystemEmergencyEvent = new PredefinedLogLevel(PredefinedLogLevels.SystemEmergencyEvent);
        public static readonly LogLevel SystemFatalEvent = new PredefinedLogLevel(PredefinedLogLevels.SystemFatalEvent);
        public static readonly LogLevel SystemAlertEvent = new PredefinedLogLevel(PredefinedLogLevels.SystemAlertEvent);
        public static readonly LogLevel SystemCriticalEvent = new PredefinedLogLevel(PredefinedLogLevels.SystemCriticalEvent);
        public static readonly LogLevel SystemSevereEvent = new PredefinedLogLevel(PredefinedLogLevels.SystemSevereEvent);

        // 64 - 127
        // 0x40 - 0x7F
        public static readonly LogLevel ApplicationError = new PredefinedLogLevel(PredefinedLogLevels.ApplicationError);
        public static readonly LogLevel ApplicationWarn = new PredefinedLogLevel(PredefinedLogLevels.ApplicationWarn);
        public static readonly LogLevel ApplicationDebug = new PredefinedLogLevel(PredefinedLogLevels.ApplicationDebug);
        public static readonly LogLevel ApplicationNotice = new PredefinedLogLevel(PredefinedLogLevels.ApplicationNotice);
        public static readonly LogLevel ApplicationInfo = new PredefinedLogLevel(PredefinedLogLevels.ApplicationInfo);

        // 128 - 191
        // 0x80 - 0xBF
        public static readonly LogLevel DeveloperTrace = new PredefinedLogLevel(PredefinedLogLevels.DeveloperTrace);
        public static readonly LogLevel DeveloperFiner = new PredefinedLogLevel(PredefinedLogLevels.DeveloperFiner);
        public static readonly LogLevel DeveloperVerbose = new PredefinedLogLevel(PredefinedLogLevels.DeveloperVerbose);
        public static readonly LogLevel DeveloperFinest = new PredefinedLogLevel(PredefinedLogLevels.DeveloperFinest);

        // 192 - 255 Custom
        // 0xC0 - 0xFF Custom

        public static readonly LogLevel Off = new PredefinedLogLevel(PredefinedLogLevels.Off);
    }
}