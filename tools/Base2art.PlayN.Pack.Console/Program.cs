﻿namespace Base2art.PlayN.Pack
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.MonkeyTail;
    using Base2art.MonkeyTail.Config;
    using Base2art.MonkeyTail.Diagnostics;
    using CommandLine;
    using Microsoft.Owin.Host.HttpListener;
    
    public class Program
    {
        private static int Main(string[] args)
        {
            return CommandLine.Parser.Default.ParseArguments<Options>(args)
                .MapResult(
                    opts => RunAppAndReturnExitCode(opts),
                    errs => 1);
            
        }

        public static int RunAppAndReturnExitCode(Options opts)
        {
            var relativeProjectPath = "project/views.json";
            
            var settings = CompilerRunner.GetBuildSettings(opts.Directory, relativeProjectPath, new SharpJsonSerializer(), true);

            var newViewSettings = new ViewsSettings();
            IViewsSettings optsSettings = opts;
            newViewSettings.Imports = optsSettings.Imports.Union(settings.Imports).ToArray();
            newViewSettings.References = optsSettings.References.Union(settings.References)
                .Select(x => new Reference { HintPath = x.HintPath, Name = x.Name })
                .ToArray();
            
            
            newViewSettings.LinkerPath = Fold(opts, settings, x => x.LinkerPath);
            newViewSettings.OutputFile = Fold(opts, settings, x => x.OutputFile);
            newViewSettings.RelativeIntermediateDirectory = Fold(opts, settings, x => x.RelativeIntermediateDirectory);
            newViewSettings.RelativeSourceDirectory = Fold(opts, settings, x => x.RelativeSourceDirectory);
            newViewSettings.SecondaryOutputFile = Fold(opts, settings, x => x.SecondaryOutputFile);
            newViewSettings.SkipSecondaryOutputFile = opts.SkipSecondaryOutputFile || settings.SkipSecondaryOutputFile;
            
            var logger = opts.Verbose
                ? (ILogger)new TextWriterLogger(Console.Out)
                : new NullLogger();
            
            
            var logFile = new StreamWriter(File.OpenWrite(Path.Combine(opts.Directory, "Logs", DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss"))));
            
            if (opts.SingleRun)
            {
                using (var compiler = CompilerRunner.StartWatching(opts.Directory, settings, new ConsoleMessenger(), logger))
                {
                    if (compiler.Compile() == CompileResult.Success)
                    {
                        return -1;
                    }
                    
                    return -2;
                }
            }
            
            Console.WriteLine("Watching Directory '{0}'...", opts.Directory);
            Console.WriteLine("Press 'Control + Z' to exit");
            
            using (var compiler = CompilerRunner.StartWatching(opts.Directory, settings, new ConsoleMessenger(), logger))
            {
                compiler.Compile();
                var isControlC = false;
                while (!isControlC)
                {
                    var keyInfo = Console.ReadKey();
                    isControlC = keyInfo.Key == ConsoleKey.Z
                        && keyInfo.Modifiers == ConsoleModifiers.Control;
                }
            }
            
            return 0;
        }

        private static string Fold(IViewsSettings opts, IViewsSettings settings, Func<IViewsSettings, string> par)
        {
            var nonDefault = par(opts);
            if (!string.IsNullOrWhiteSpace(nonDefault))
            {
                return nonDefault;
            }
            
            return par(settings);
        }
    }
}
