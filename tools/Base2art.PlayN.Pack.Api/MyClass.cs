﻿

namespace Base2art.PlayN.Pack.Api
{
    public interface ICompiler
    {
        string[] ExtensionFilter { get; }
    }
}