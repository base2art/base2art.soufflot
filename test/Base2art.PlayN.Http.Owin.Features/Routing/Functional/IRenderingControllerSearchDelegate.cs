﻿namespace Base2art.PlayN.Api.Routing.Functional
{
    using System;

    using Base2art.PlayN.Http;

    public interface IRenderingControllerSearchDelegate
    {
        Type FindType(IHttpRequest request);
    }
}