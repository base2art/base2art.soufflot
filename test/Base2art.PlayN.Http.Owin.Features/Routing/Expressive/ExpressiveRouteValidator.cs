namespace Base2art.PlayN.Api.Routing.Expressive
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    using Base2art.PlayN.Http;
    using Base2art.PlayN.Mvc;

    public class ExpressiveRouteValidator<TController> : ExpressiveRouteValidatorBase<TController>
        where TController : IRenderingController
    {
        public RouteExpressionTree ValidateExpression(
            Expression<Func<TController, IHttpContext, List<PositionedResult>, IResult>> func)
        {
            return this.VerifyNonFunctionalExpression(func);
        }

        public RouteExpressionTree ValidateNonFunctionalExpression(Expression func)
        {
            return this.VerifyNonFunctionalExpression(func);
        }
    }
}