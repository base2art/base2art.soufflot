﻿namespace Base2art.PlayN.Api.Routing.Expressive
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    using Base2art.PlayN.Http;
    using Base2art.PlayN.Mvc;

    public class ExpressiveRouteData<T> : RouteData<T>
    {
        public ExpressiveRouteData(IClass<T> controllerClass, Expression<Func<IRenderingController, IHttpContext, List<PositionedResult>, IResult>> expression)
            : base(controllerClass, expression)
        {
        }
    }
}