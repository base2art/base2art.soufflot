﻿namespace Base2art.PlayN.Api.Routing.Expressive
{
    using Base2art.PlayN.Mvc;

    public interface IExpressiveReverseRouter
    {
        IRoutable<TController> For<TController>()
            where TController : IRenderingController;

    }
}