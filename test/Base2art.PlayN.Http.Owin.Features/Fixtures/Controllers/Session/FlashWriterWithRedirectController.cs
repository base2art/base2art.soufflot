namespace Base2art.PlayN.Samples.Session
{
    using System.Collections.Generic;
    using System.Linq;

    using Base2art.PlayN.Api;
    using Base2art.PlayN.Http;
    using Base2art.PlayN.Mvc;

    public class FlashWriterWithRedirectController : SimpleRenderingController
    {
        protected override IEnumerable<INonRenderingController> NonRenderingControllers
        {
            get
            {
                yield return new SubController();
            }
        }

        protected override IResult ExecuteMain(IHttpContext httpContext, List<PositionedResult> childResults)
        {
            return httpContext.Redirect((string)null);
        }

        private class SubController : SimpleNonRenderingController
        {
            protected override void ExecuteMain(IHttpContext httpContext)
            {
                IHttpQueryString httpQueryString = httpContext.Request.QueryString;

                if (httpQueryString.Contains("key-name") && httpQueryString.Contains("value"))
                {
                    string keyName = httpQueryString["key-name"].FirstOrDefault() ?? "value";
                    string value = httpQueryString["value"].FirstOrDefault() ?? string.Empty;
                    httpContext.Flash[keyName] = value;
                }
            }
        }
    }
}