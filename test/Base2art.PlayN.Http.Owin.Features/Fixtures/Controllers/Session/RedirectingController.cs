﻿namespace Base2art.PlayN.Samples.Session
{
    using System.Collections.Generic;

    using Base2art.PlayN.Api;
    using Base2art.PlayN.Http;
    using Base2art.PlayN.Http.Util;
    using Base2art.PlayN.Mvc;

    public class RedirectingController : SimpleRenderingController
    {
        protected override IResult ExecuteMain(IHttpContext httpContext, List<PositionedResult> childResults)
        {
            var value = httpContext.Request.QueryString.GetFirstOrEmpty("dest");
            return httpContext.Redirect(value);
        }
    }
}