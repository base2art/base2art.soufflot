﻿//namespace Base2art.PlayN.Features
//{
//    using System;
//    using System.Web;
//
//    using Base2art.MockServer;
//    using Base2art.MockUI;
//    using Base2art.MockUI.CsQuery;
//
//    using FluentAssertions;
//    using NUnit.Framework;
//
//    [TestFixture]
//    [Ignore]
//    public class GetAspxPages
//    {
//        private readonly MockServer mockServer = new MockServer(new ServerData
//                                                                    {
//                                                                        PhysicalApplicationDirectory = @"C:\Users\syoungblut\db-2\Dropbox\Base2art.PlayN\data\Base2art.PlayN.Sample3\"
//                                                                    });
//
//        [Test]
//        public void ShouldGetPageDirectly()
//        {
//            using (var browser = new MockBrowser(new CsQueryFormParser(), this.mockServer))
//            {
//                new Action(() => browser.RequestPage(new PageData { Path = "/sites/www.base2art.com/index.aspx" }))
//                    .ShouldThrow<ClientErrorException>().And.StatusCode.Should().Be(403);
//            }
//        }
//
//        [Test]
//        public void ShouldGetPageByVanityDomain()
//        {
//            using (var browser = new MockBrowser(new CsQueryFormParser(), this.mockServer))
//            {
//                IQueryablePageResult result = browser.RequestPage(new PageData { VanityHost = "www.base2art.com", Path = "/" });
//                result.RawText.Trim().Should().Be("base2art domain: Index page");
//            }
//        }
//
//        [Test]
//        public void ShouldGetPageByVanityDomainWithHtmlExtension()
//        {
//            using (var browser = new MockBrowser(new CsQueryFormParser(), this.mockServer))
//            {
//                IQueryablePageResult result = browser.RequestPage(new PageData { VanityHost = "www.base2art.com", Path = "/about.html" });
//                result.RawText.Trim().Should().Contain("base2art domain: About page");
//                result.RawText.Trim().Should().NotContain("/sites/www.base2art.com/about.aspx");
//                result.RawText.Trim().Should().Contain("/about.html");
//            }
//        }
//
//        [Test]
//        public void ShouldGetPageByVanityDomainWithIndexHtmlShouldThrowNotFound()
//        {
//            using (var browser = new MockBrowser(new CsQueryFormParser(), this.mockServer))
//            {
//                new Action(() => browser.RequestPage(new PageData { VanityHost = "www.base2art.com", Path = "/index.html" }))
//                    .ShouldThrow<ResourceNotFoundException>().And.StatusCode.Should().Be(404);
//            }
//        }
//
//        [Test]
//        public void ShouldGetPageByNoneDefinedFallsBackForDebugging()
//        {
//            using (var browser = new MockBrowser(new CsQueryFormParser(), this.mockServer))
//            {
//                IQueryablePageResult result = browser.RequestPage(new PageData { Path = "/" });
//                result.RawText.Trim().Should().Be("base2art domain: Index page");
//            }
//        }
//
//        [Test]
//        public void ShouldSupportSharedPages()
//        {
//            using (var browser = new MockBrowser(new CsQueryFormParser(), this.mockServer))
//            {
//                IQueryablePageResult result = browser.RequestPage(new PageData { Path = "page1.html" });
//                result.RawText.Trim().Should().Be("SHARED SITE: PAGE 1");
//            }
//        }
//
//        [Test]
//        public void ShouldSupportVanityPathsMultipleVanitiesResolveToSameDiskPath()
//        {
//            using (var browser = new MockBrowser(new CsQueryFormParser(), this.mockServer))
//            {
//                // sjy.html should resolve to about-us.aspx
//                IQueryablePageResult result = browser.RequestPage(new PageData { Path = "sjy.html" });
//                result.RawText.Trim().Should().Contain("base2art domain: About page");
//                result.RawText.Trim().Should().NotContain("/sites/www.base2art.com/about.aspx");
//                result.RawText.Trim().Should().Contain("/sjy.html");
//            }
//        }
//
//        [Test]
//        public void ShouldSupportRedirectsTemp()
//        {
//            using (var browser = new MockBrowser(new CsQueryFormParser(), this.mockServer))
//            {
//                // sjy.html should resolve to about-us.aspx
//                var exc = new Action(() => browser.RequestPage(new PageData { Path = "301-to-google.html" }))
//                    .ShouldThrow<RedirectException>().And;
//                exc.RedirectLocation.Should().Be("http://www.google.com/");
//                exc.PageResult.StatusCode.Should().Be(301);
//            }
//        }
//
//        [Test]
//        public void ShouldSupportRedirectsPerm()
//        {
//            using (var browser = new MockBrowser(new CsQueryFormParser(), this.mockServer))
//            {
//                // sjy.html should resolve to about-us.aspx
//                var exc = new Action(() => browser.RequestPage(new PageData { Path = "302-to-google.html" }))
//                    .ShouldThrow<RedirectException>().And;
//                exc.RedirectLocation.Should().Be("http://www.yahoo.com/");
//                exc.PageResult.StatusCode.Should().Be(302);
//            }
//        }
//
//        [Test]
//        public void ShouldGet404PageWithCustomErrorsOn()
//        {
//            using (var browser = new MockBrowser(new CsQueryFormParser(), this.mockServer))
//            {
//                // sjy.html should resolve to about-us.aspx
//                // , ContextSetup = new CustomErrorsOnContextSetup()
//                var exception = new Action(() => browser.RequestPage(new PageData
//                {
//                    Path = "NO_PAGE.html",
//                }))
//                    .ShouldThrow<ResourceNotFoundException>();
//
//                Console.WriteLine(exception.And);
//                exception.And.PageResult.RawText.Trim().Should().Contain("base2art domain: ERROR 404 page");
//                exception.And.PageResult.RawText.Trim().Should().NotContainEquivalentOf("/sites/www.base2art.com/_Error_404.aspx");
//                exception.And.PageResult.RawText.Trim().Should().ContainEquivalentOf("/NO_PAGE.html");
//            }
//        }
//
//        [Test]
//        public void ShouldGet500PageWithCustomErrorsOn()
//        {
//            using (var browser = new MockBrowser(new CsQueryFormParser(), this.mockServer))
//            {
//                // sjy.html should resolve to about-us.aspx
//                // , ContextSetup = new CustomErrorsOnContextSetup()
//                var exception = new Action(() => browser.RequestPage(new PageData
//                {
//                    Path = "error.html",
//                }))
//                    .ShouldThrow<ServerErrorException>();
//
//                exception.And.PageResult.RawText.Trim().Should().ContainEquivalentOf("SHARED domain: ERROR 500 page");
//                exception.And.PageResult.RawText.Trim().Should().NotContainEquivalentOf("/sites/www.base2art.com/_Error.aspx");
//                exception.And.PageResult.RawText.Trim().Should().ContainEquivalentOf("/error.html");
//            }
//        }
//
//        [Test]
//        public void ShouldGet400WhenRequestPageThatStartsWithUnderscore()
//        {
//            using (var browser = new MockBrowser(new CsQueryFormParser(), this.mockServer))
//            {
//                new Action(() => browser.RequestPage(new PageData
//                {
//                    Path = "_error.html",
//                }))
//                    .ShouldThrow<ResourceNotFoundException>().And.
//                    PageResult.StatusCode.Should().Be(404);
//
//                new Action(() => browser.RequestPage(new PageData
//                {
//                    Path = "/level-two/_error.html",
//                }))
//                    .ShouldThrow<ResourceNotFoundException>().And.
//                    PageResult.StatusCode.Should().Be(404);
//            }
//        }
//    }
//}
