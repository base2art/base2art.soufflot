namespace Base2art.PlayN.Api.Fixtures
{
    using Base2art.PlayN.Mvc;

    using Base2art.PlayN.Http;

    public class SubCountingNonRenderingController : INonRenderingController
    {
        public static int Count = 0;
        public INonRenderingController[] NonRenderingControllers
        {
            get
            {
                return new INonRenderingController[0];
            }
        }

        public void Execute(IHttpContext context)
        {
            Count++;
        }
    }
}