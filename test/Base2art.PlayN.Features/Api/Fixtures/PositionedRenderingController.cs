﻿namespace Base2art.PlayN.Api.Fixtures
{
    using Base2art.PlayN.Mvc;

    public class PositionedRenderingController : IPositionedRenderingController
    {
        public IRenderingController RenderingController { get; set; }

        public int Container { get; set; }

        public int ContainerPriority { get; set; }
    }
}