namespace Base2art.PlayN.Api.Fixtures
{
    using Base2art.PlayN.Mvc;

    using Base2art.PlayN.Http;

    public class CountingNonRenderingController : INonRenderingController
    {
        public static int Count = 0;
        public INonRenderingController[] NonRenderingControllers
        {
            get
            {
                return new INonRenderingController[] { new SubCountingNonRenderingController() };
            }
        }

        public void Execute(IHttpContext context)
        {
            Count++;
        }
    }
}